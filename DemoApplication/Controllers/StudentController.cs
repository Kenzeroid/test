﻿using DemoApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoApplication.Controllers
{
    public class StudentController : Controller
    {
        public ActionResult Details(int Id)
        {
            Student std = ListStudent.Where(s => s.Id == Id).FirstOrDefault();
            return View(std);
        }
        [HttpPost]
        public ActionResult Details(Student model)
        {
            return Redirect("../Home");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Student model)
        {
            int newId = ListStudent.Max(o => o.Id) + 1;
            model.Id = newId;
            ListStudent.Add(model);
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int Id)
        {
            Student std = ListStudent.Where(s => s.Id == Id).FirstOrDefault();
            return View(std);
        }
        [HttpPost]
        public ActionResult Delete(Student std)
        {
            ListStudent.RemoveAt(std.Id - 1);
            return RedirectToAction("Index");
        }


        public ActionResult Edit(int Id)
        {
            Student std = ListStudent.Where(s => s.Id == Id).FirstOrDefault();
            return View(std);
        }
        [HttpPost]
        public ActionResult Edit(Student model)
        {
            string firstName = model.FirstName;
            string lastName = model.LastName;
            string gender = model.Gender;
            int age = model.Age;

            ListStudent[model.Id - 1].FirstName = firstName;
            ListStudent[model.Id - 1].LastName = lastName;
            ListStudent[model.Id - 1].Gender = gender;
            ListStudent[model.Id - 1].Age = age;
            return RedirectToAction("Index");
        }
        // GET: Student
        public static List<Student> ListStudent = new List<Student>()
        {
            new Student
            {
                Id = 1, FirstName = "Atur", LastName = "Aritonang", Gender = "Male", Age = 27
            },
            new Student
            {
                Id = 2, FirstName = "John", LastName = "Phillip", Gender = "Male", Age = 12
            },
            new Student
            {
                Id = 3, FirstName = "Queen", LastName = "Zefanya", Gender = "Female", Age = 15
            }
        };
        public ActionResult Index()
        {
            return View(ListStudent);
        }
    }
}