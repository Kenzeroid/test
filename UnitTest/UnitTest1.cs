﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XPOS.DataAccess;
using XPOS.DataModel;
using XPOS.ViewModell;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestInsertCategory()
        {
            Trace.WriteLine("--- Start Testing Insert Category Repo ---");
            CategoryViewModel category = new CategoryViewModel();
            category.Initial = "TST";
            category.Name = "Testing";

            ResponseResult respon = CategoryRepo.Update(category);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Save Successfull ---");

                CategoryViewModel catRespon = (CategoryViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1}",
                    catRespon.Initial, catRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Save Failed ---");
                Trace.WriteLine("fail message : " + respon.Message);
            }


            Trace.WriteLine("--- End Testing Insert Category Repo ---");
        }

        [TestMethod]
        public void TestCategoryRepo()
        {
            Trace.WriteLine("--- Start Testing Category Repo ---");
            List<CategoryViewModel> result = CategoryRepo.All();

            foreach (var item in result)
            {
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1}",
                    item.Initial, item.Name));
            }
            Trace.WriteLine("--- End Testing Category Repo ---");
        }

        [TestMethod]
        public void TestCategoryById()
        {
            Trace.WriteLine("--- Start Testing Category By Id Repo ---");
            CategoryViewModel result = CategoryRepo.byId(10);
            
            Trace.WriteLine(String.Format("Id : {2}, Initial : {0}, Name : {1}",
                    result.Initial, result.Name, result.Id));

            Trace.WriteLine("--- End Testing Category By Id Repo ---");
        }

        [TestMethod]
        public void TestCategoryUpdate()
        {
            Trace.WriteLine("--- Start Testing Update Category Repo ---");
            CategoryViewModel category = new CategoryViewModel();
            category.Id = 10;
            category.Name = "Testing";

            ResponseResult respon = CategoryRepo.Update(category);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Save Successfull ---");

                CategoryViewModel catRespon = (CategoryViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1}",
                    catRespon.Initial, catRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Save Failed ---");
                Trace.WriteLine("fail message : " + respon.Message);
            }


            Trace.WriteLine("--- End Testing Update Category Repo ---");
        }

        [TestMethod]
        public void TestCategoryDelete()
        {
            Trace.WriteLine("--- Start Testing Delete Category Repo ---");
            CategoryViewModel category =  CategoryRepo.byId(5);

            ResponseResult respon = CategoryRepo.Delete(category);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Delete Successfull ---");

                CategoryViewModel catRespon = (CategoryViewModel)respon.Entity;

                Trace.WriteLine(String.Format("Initial : {0}, Name : {1}",
                    catRespon.Initial, catRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Delete Failed ---");
                Trace.WriteLine("fail message : " + respon.Message);
            }


            Trace.WriteLine("--- End Testing Delete Category Repo ---");
        }
    }
}
