﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XPOS.DataAccess;
using XPOS.DataModel;
using XPOS.ViewModell;

namespace UnitTest
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void TestInsertVariant()
        {
            Trace.WriteLine("--- Start Testing Insert Variant Repo ---");
            VariantViewModel variant = new VariantViewModel();
            variant.Initial = "TST";
            variant.Name = "Testing";

            ResponseResult respon = VariantRepo.Update(variant);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Save Successfull ---");

                VariantViewModel catRespon = (VariantViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1}",
                    catRespon.Initial, catRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Save Failed ---");
                Trace.WriteLine("fail message : " + respon.Message);
            }


            Trace.WriteLine("--- End Testing Insert Variant Repo ---");
        }

        /*[TestMethod]
        public void TestVariantRepo()
        {
            Trace.WriteLine("--- Start Testing Variant Repo ---");
            List<VariantViewModel> result = VariantRepo.All();

            foreach (var item in result)
            {
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1}",
                    item.Initial, item.Name));
            }
            Trace.WriteLine("--- End Testing Variant Repo ---");
        }*/

        [TestMethod]
        public void VariantById()
        {
            Trace.WriteLine("--- Start Testing Variant By Id Repo ---");
            VariantViewModel result = VariantRepo.byId(10);

            Trace.WriteLine(String.Format("Id : {2}, Initial : {0}, Name : {1}",
                    result.Initial, result.Name, result.Id));

            Trace.WriteLine("--- End Testing Variant By Id Repo ---");
        }

        [TestMethod]
        public void TestVariantUpdate()
        {
            Trace.WriteLine("--- Start Testing Update Variant Repo ---");
            VariantViewModel variant = new VariantViewModel();
            variant.Id = 10;
            variant.Name = "Testing";

            ResponseResult respon = VariantRepo.Update(variant);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Save Successfull ---");

                VariantViewModel catRespon = (VariantViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1}",
                    catRespon.Initial, catRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Save Failed ---");
                Trace.WriteLine("fail message : " + respon.Message);
            }


            Trace.WriteLine("--- End Testing Update Variant Repo ---");
        }

        [TestMethod]
        public void TestVariantDelete()
        {
            Trace.WriteLine("--- Start Testing Delete Variant Repo ---");
            VariantViewModel variant = VariantRepo.byId(5);

            ResponseResult respon = VariantRepo.Delete(variant);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Delete Successfull ---");

                VariantViewModel catRespon = (VariantViewModel)respon.Entity;

                Trace.WriteLine(String.Format("Initial : {0}, Name : {1}",
                    catRespon.Initial, catRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Delete Failed ---");
                Trace.WriteLine("fail message : " + respon.Message);
            }


            Trace.WriteLine("--- End Testing Delete Variant Repo ---");
        }
    }
}
