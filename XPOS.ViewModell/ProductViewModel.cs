﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XPOS.ViewModell
{
    public class ProductViewModel
    {
        public long Id { get; set; }
        public long CategoryId { get; set; }
        public long VariantId { get; set; }

        [Required]
        [StringLength(10)]
        public string Initial { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        public decimal Price { get; set; }

        public bool Active { get; set; }


    }
}
