﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPOS.DataModel;
using XPOS.ViewModell;

namespace XPOS.DataAccess
{
    public class CategoryRepo
    {
        //Get All
        public static List<CategoryViewModel> All()
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPosContext())
            {
                result = (from c in db.Categories
                    select new CategoryViewModel
                    {
                        Id = c.Id,
                        Initial = c.Initial,
                        Name = c.Name,
                        Active = c.Active
                    }).ToList();
            }
            return result;
        }

        //Get by Id
        public static CategoryViewModel byId(int Id)
        {
            //id Category.Id
            CategoryViewModel result = new CategoryViewModel();
            using (var db = new XPosContext())
            {
                result = (from c in db.Categories
                          where c.Id == Id
                          select new CategoryViewModel
                          {
                              Id = c.Id,
                              Initial = c.Initial,
                              Name = c.Name,
                              Active = c.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new CategoryViewModel();
        }

        // Create menu & edit
        public static ResponseResult Update(CategoryViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region Create New / Insert 
                    if (entity.Id == 0)
                    {
                        Category category = new Category();

                        category.Initial = entity.Initial;
                        category.Name = entity.Name;
                        category.Active = entity.Active;

                        category.CreateBy = "Your Name";
                        category.CreateDate = DateTime.Now;

                        db.Categories.Add(category);
                        db.SaveChanges();

                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        Category category = db.Categories
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault();

                        if (category != null)
                        {
                            category.Initial = entity.Initial;
                            category.Name = entity.Name;
                            category.Active = entity.Active;

                            category.ModifyBy = "Your Name";
                            category.ModifyDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Category not found";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        
        // Delete
        public static ResponseResult Delete(CategoryViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Category category = db.Categories
                    .Where(o => o.Id == entity.Id)
                    .FirstOrDefault();
                    
                    if (category != null)
                    {
                        db.Categories.Remove(category);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Category not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
