﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPOS.DataModel;
using XPOS.ViewModell;

namespace XPOS.DataAccess
{
    public class VariantRepo
    {
        //Get All

        public static List<VariantViewModel> All()
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = (from c in db.Variants
                          select new VariantViewModel
                          {
                              Id = c.Id,
                              CategoryId = c.CategoryId,
                              Initial = c.Initial,
                              Name = c.Name,
                              Active = c.Active
                          }).ToList();
            }
            return result;
        }
        public static List<VariantViewModel> ByCategory(long categoryId)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = (from c in db.Variants
                          join d in db.Categories
                          on c.CategoryId equals d.Id
                          where c.CategoryId == categoryId
                          select new VariantViewModel
                          {
                              Id = c.Id,
                              CategoryId = c.CategoryId,
                              Initial = c.Initial,
                              Name = c.Name,
                              Active = c.Active
                          }).ToList();
            }
            return result;
        }


        //Get by Id
        public static VariantViewModel byId(long Id)
        {
            //id Variant.Id
            VariantViewModel result = new VariantViewModel();
            using (var db = new XPosContext())
            {
                result = (from c in db.Variants
                          where c.Id == Id
                          select new VariantViewModel
                          {
                              Id = c.Id,
                              CategoryId = c.CategoryId,
                              Initial = c.Initial,
                              Name = c.Name,
                              Active = c.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new VariantViewModel();
        }
        // Create menu & edit
        public static ResponseResult Update(VariantViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region Create New / Insert 
                    if (entity.Id == 0)
                    {
                        Variant variant = new Variant();

                        variant.CategoryId = entity.CategoryId;
                        variant.Initial = entity.Initial;
                        variant.Name = entity.Name;
                        variant.Active = entity.Active;

                        variant.CreateBy = "Your Name";
                        variant.CreateDate = DateTime.Now;

                        db.Variants.Add(variant);
                        db.SaveChanges();

                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                        Variant variant = db.Variants
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault();

                        if (variant != null)
                        {
                            variant.CategoryId = entity.CategoryId;
                            variant.Initial = entity.Initial;
                            variant.Name = entity.Name;
                            variant.Active = entity.Active;

                            variant.ModifyBy = "Your Name";
                            variant.ModifyDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Variant not found";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        // Delete
        public static ResponseResult Delete(VariantViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Variant variant = db.Variants
                    .Where(o => o.Id == entity.Id)
                    .FirstOrDefault();

                    if (variant != null)
                    {
                        db.Variants.Remove(variant);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Variant not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
