﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPOS.DataModel;
using XPOS.ViewModell;

namespace XPOS.DataAccess
{
    public class ProductRepo
    {
        //Get All

        public static List<ProductViewModel> All()
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = (from c in db.Products
                          select new ProductViewModel
                          {
                              Id = c.Id,
                              VariantId = c.VariantId,
                              Initial = c.Initial,
                              Name = c.Name,
                              Description = c.Description,
                              Price = c.Price,
                              Active = c.Active
                          }).ToList();
            }
            return result;
        }
        public static List<ProductViewModel> GetByCategory(long variantId)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = (from c in db.Products
                          join d in db.Variants
                          on c.VariantId equals d.Id
                          where c.VariantId == variantId
                          select new ProductViewModel
                          {
                              Id = c.Id,
                              VariantId = c.VariantId,
                              Initial = c.Initial,
                              Name = c.Name,
                              Description = c.Description,
                              Price = c.Price,
                              Active = c.Active
                          }).ToList();
            }
            return result;
        }


        //Get by Id
        public static ProductViewModel byId(long Id)
        {
            //id Product.Id
            ProductViewModel result = new ProductViewModel();
            using (var db = new XPosContext())
            {
                result = (from c in db.Products
                          join d in db.Variants on c.VariantId equals d.Id  into varlf
                          from d in varlf.DefaultIfEmpty()
                          join e in db.Categories on d.CategoryId equals e.Id into catlf
                          from e in catlf.DefaultIfEmpty()
                          where c.Id == Id
                          select new ProductViewModel
                          {
                              Id = c.Id,
                              VariantId = c.VariantId,
                              Initial = c.Initial,
                              Name = c.Name,
                              Description = c.Description,
                              Price = c.Price,
                              Active = c.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new ProductViewModel();
        }
        // Create menu & edit
        public static ResponseResult Update(ProductViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region Create New / Insert 
                    if (entity.Id == 0)
                    {
                        Product product = new Product();

                        product.VariantId = entity.VariantId;
                        product.Initial = entity.Initial;
                        product.Name = entity.Name;
                        product.Description = entity.Description;
                        product.Price = entity.Price;
                        product.Active = entity.Active;

                        product.CreateBy = "Your Name";
                        product.CreateDate = DateTime.Now;

                        db.Products.Add(product);
                        db.SaveChanges();

                        result.Entity = entity;
                    }

                    #endregion
                    #region Edit

                    else
                    {
                       Product product = db.Products
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault();

                        if (product != null)
                        {
                            product.VariantId = entity.VariantId;
                            product.Initial = entity.Initial;
                            product.Name = entity.Name;
                            product.Description = entity.Description;
                            product.Price = entity.Price;
                            product.Active = entity.Active;

                            product.CreateBy = "Your Name";
                            product.CreateDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Product not found";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
        // Delete
        public static ResponseResult Delete(ProductViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Product product = db.Products
                    .Where(o => o.Id == entity.Id)
                    .FirstOrDefault();

                    if (product != null)
                    {
                        db.Products.Remove(product);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Product not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
