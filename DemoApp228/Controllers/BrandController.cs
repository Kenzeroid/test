﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPOS.DataModel;

namespace DemoApp228.Controllers
{
    public class BrandController : Controller
    {
        // GET: Brand
        public ActionResult Index()
        {
            List<Brand> brand = new List<Brand>();
            using (XPOSContext db = new XPOSContext())
            {
                brand = db.Brands.Where(a => a.Active == true).ToList();
            }
            return View(brand);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Brand brand)
        {
            brand.CreatedBy = "A";
            brand.CreatedAt = System.DateTime.Now;
            brand.ModifiedBy = "A";
            brand.ModifiedAt = System.DateTime.Now;
            brand.IsDelete = false;
            using (XPOS.DataModel.XPOSContext db = new XPOSContext())
            {
                db.Brands.Add(brand);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            Brand brand = new Brand();
            using (XPOSContext db = new XPOSContext())
            {
                brand = db.Brands.Where(a => a.Id == Id).FirstOrDefault();
            }
            return View(brand);
        }

        [HttpPost]
        public ActionResult Edit(Category brand)
        {
            Brand brand1 = new Brand();
            using (XPOSContext db = new XPOSContext())
            {
                brand1 = db.Brands.Where(a => a.Id == brand.Id).FirstOrDefault();

                brand1.Initial = brand.initial;
                brand1.Name = brand.Name;
                brand1.Active = brand.Active;
                brand1.ModifiedAt = System.DateTime.Now;
                db.Entry(brand1).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("index");
        }
        // [HttpPost]
        public ActionResult Delete(int Id)
        {
            Brand brand = new Brand();
            using (XPOSContext db = new XPOSContext())
            {
                brand = db.Brands.Where(a => a.Id == Id).FirstOrDefault();
                brand.IsDelete = true;
                brand.Active = false;
                brand.ModifiedAt = System.DateTime.Now;
                db.Entry(brand).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}