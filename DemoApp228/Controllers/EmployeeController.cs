﻿using DemoApp228.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoApp228.Controllers
{
    public class EmployeeController : Controller
    {
        public static List<Employee> ListEmployee = new List<Employee>()
        { new Employee {
            id = 1, FirstName = "Atur", LastName = "Aritonang",
            Position = "Trainer" , Salary = 10000000 , Gender = "Male" },
          new Employee {
            id = 2, FirstName = "John", LastName = "Phillip",
            Position = "Trainer" , Salary = 10000000 , Gender = "Male" },
          new Employee {
            id = 3, FirstName = "Queen", LastName = "Zefanya",
            Position = "Trainee" , Salary = 5000000 , Gender = "Female" }
        };

        // GET: Employee
        public ActionResult Index()
        {
            return View(ListEmployee);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Employee model)
        {
            int newId = ListEmployee.Max(o => o.id) + 1;
            model.id = newId;
            ListEmployee.Add(model);

            return RedirectToAction("Index");
        }
    }
}