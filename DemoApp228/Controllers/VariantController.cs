﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPOS.DataAccess;
using XPOS.ViewModell;

namespace DemoApp228.Controllers
{
    public class VariantController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            return PartialView("_List", VariantRepo.All());
        }
        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.All(), "Id", "Name");
            return PartialView("_Create", new VariantViewModel());
        }

        [HttpPost]
        public ActionResult Create(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id)
        {
            // id => Category Id
            VariantViewModel model = VariantRepo.byId(id);
            ViewBag.CategoryList = new SelectList(CategoryRepo.All(), "Id", "Name");
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            // id => Category Id
            VariantViewModel model = VariantRepo.byId(id);
            ViewBag.CategoryList = new SelectList(CategoryRepo.All(), "Id", "Name");
            return PartialView("_Delete", model);
        }

        [HttpPost]
        public ActionResult Delete(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}