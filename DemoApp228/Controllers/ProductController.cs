﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPOS.DataAccess;
using XPOS.ViewModell;

namespace DemoApp228.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            return PartialView("_List", ProductRepo.All());
        }
        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.All(), "Id", "Name");
            ViewBag.VariantList = new SelectList(VariantRepo.ByCategory(0),
                "Id", "Name");
            return PartialView("_Create", new ProductViewModel());
        }

        public ActionResult GetByCategory(long categoryId)
        {
            return View(VariantRepo.ByCategory(categoryId));
        }

        [HttpPost]
        public ActionResult Create(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(long id)
        {
            // id => Category Id
            ProductViewModel model = ProductRepo.byId(id);
            ViewBag.CategoryList = new SelectList(CategoryRepo.All(), "Id", "Name");
            ViewBag.VariantList = new SelectList(VariantRepo.ByCategory(model.CategoryId),
                "Id", "Name");
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            // id => Category Id
            
            return PartialView("_Delete", ProductRepo.byId(id));
        }

        [HttpPost]
        public ActionResult Delete(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }
    }
}