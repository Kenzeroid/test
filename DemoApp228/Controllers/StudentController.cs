﻿using DemoApp228.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoApp228.Controllers
{
    public class StudentController : Controller
    {
        public static List<Student> ListStudent = new List<Student>()
        { new Student {
            id = 1, FirstName = "Atur", LastName = "Aritonang",
            Gender = "Male", Age = 27 },
          new Student {
            id = 2, FirstName = "John", LastName = "Phillip",
            Gender = "Male", Age = 12 },
          new Student {
            id = 3, FirstName = "Queen", LastName = "Zefanya",
            Gender = "Female", Age = 15 }
        };

        // GET: Student
        public ActionResult Index()
        {
            return View(ListStudent);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Student model)
        {
            int newId = ListStudent.Max(o => o.id) + 1;
            model.id = newId;
            ListStudent.Add(model);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = ListStudent.Where(o => o.id == Id).FirstOrDefault();
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Student model)
        {
            var FirstName = model.FirstName;
            var LastName = model.LastName;
            var Gender = model.Gender;
            var age = model.Age;

            ListStudent[model.id - 1].FirstName = FirstName;
            ListStudent[model.id - 1].LastName = LastName;
            ListStudent[model.id - 1].Gender = Gender;
            ListStudent[model.id - 1].Age = age;

            return RedirectToAction("index");
        }

        //[HttpGet]
        //public ActionResult Delete(int Id, bool saveChangesError = false)
        //{
        //    if (Id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    if (saveChangesError.GetValueOrDefault())
        //    {
        //        ViewBag.ErrorMessage = "Delete failed. Try again, and if the problem persists see your system administrator.";
        //    }
        //    Student student = db.Students.Find(Id);
        //    if (student == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(student);
        
    }
}