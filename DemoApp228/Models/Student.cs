﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoApp228.Models
{
    public class Student
    {
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
    }
}