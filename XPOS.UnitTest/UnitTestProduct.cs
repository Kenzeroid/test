﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XPOS.DataAccess;
using XPOS.ViewModel;

namespace XPOS.UnitTest
{
    [TestClass]
    public class UnitTestProduct
    {
        [TestMethod]
        public void TestInsertProduct()
        {
            Trace.WriteLine("--- Start Testing Insert Product Repo ---");
            ProductViewModel product = new ProductViewModel();
            product.Initial = "TST";
            product.Name = "Testing";
            ResponseResult respon = ProductRepo.Update(product);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Save Success ---");
                ProductViewModel proRespon = (ProductViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", proRespon.Initial, proRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Save failed ---");
                Trace.WriteLine("Fail message : " + respon.Message);
            }
            Trace.WriteLine("--- End Testing Insert Product Repo ---");
        }

        [TestMethod]
        public void TestProductRepo()
        {
            Trace.WriteLine("--- Start Testing Product Repo ---");
            List<ProductViewModel> result = ProductRepo.All();
            foreach (var item in result)
            {
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", item.Initial, item.Name));
            }
            Trace.WriteLine("--- End Testing Product Repo ---");
        }

        [TestMethod]
        public void TestProductById()
        {
            Trace.WriteLine("--- Start Testing Category Repo ---");
            ProductViewModel result = ProductRepo.byId(10);
            Trace.WriteLine(String.Format("Id : {2} Initial : {0}, Name : {1} ", result.Initial, result.Name, result.Id));
            Trace.WriteLine("--- End Testing Category Repo ---");
        }

        [TestMethod]
        public void TestCategoryUpdate()
        {
            Trace.WriteLine("--- Start Testing Update Product Repo ---");
            ProductViewModel product = new ProductViewModel();
            //category.Initial = "TST";
            product.Name = "Testing";
            product.Id = 10;
            ResponseResult respon = ProductRepo.Update(product);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Save Successful ---");
                ProductViewModel proRespon = (ProductViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", proRespon.Initial, proRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Save failed ---");
                Trace.WriteLine("Fail message : " + respon.Message);
            }
            Trace.WriteLine("--- End Testing Update Product Repo ---");
        }

        [TestMethod]
        public void TestCategoryDelete()
        {
            Trace.WriteLine("--- Start Testing Delete Product Repo ---");
            ProductViewModel product = ProductRepo.byId(5);
            ResponseResult respon = ProductRepo.Delete(product);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Delete Successful ---");
                ProductViewModel proRespon = (ProductViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", proRespon.Initial, proRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Delete failed ---");
                Trace.WriteLine("Fail message : " + respon.Message);
            }
            Trace.WriteLine("--- End Testing Delete Category Repo ---");
        }
    }
}
