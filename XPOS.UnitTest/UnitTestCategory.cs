﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XPOS.DataAccess;
using XPOS.ViewModel;

namespace XPOS.UnitTest
{
    [TestClass]
    public class UnitTestCategory
    {
        [TestMethod]
        public void TestInsertCategory()
        {
            Trace.WriteLine("--- Start Testing Insert Category Repo ---");
            CategoryViewModel category = new CategoryViewModel();
            category.Initial = "TST";
            category.Name = "Testing";
            ResponseResult respon = CategoryRepo.Update(category);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Save Success ---");
                CategoryViewModel catRespon = (CategoryViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", catRespon.Initial, catRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Save failed ---");
                Trace.WriteLine("Fail message : " + respon.Message);
            }
            Trace.WriteLine("--- End Testing Insert Category Repo ---");
        }

        [TestMethod]
        public void TestCategoryRepo()
        {
            Trace.WriteLine("--- Start Testing Category Repo ---");
            List<CategoryViewModel> result = CategoryRepo.All();
            foreach (var item in result)
            {
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", item.Initial, item.Name));
            }
            Trace.WriteLine("--- End Testing Category Repo ---");
        }

        [TestMethod]
        public void TestCategoryById()
        {
            Trace.WriteLine("--- Start Testing Category Repo ---");
            CategoryViewModel result = CategoryRepo.byId(10);
            Trace.WriteLine(String.Format("Id : {2} Initial : {0}, Name : {1} ", result.Initial, result.Name, result.Id));
            Trace.WriteLine("--- End Testing Category Repo ---");
        }

        [TestMethod]
        public void TestCategoryUpdate()
        {
            Trace.WriteLine("--- Start Testing Update Category Repo ---");
            CategoryViewModel category = new CategoryViewModel();
            //category.Initial = "TST";
            category.Name = "Testing";
            category.Id = 10;
            ResponseResult respon = CategoryRepo.Update(category);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Save Successful ---");
                CategoryViewModel catRespon = (CategoryViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", catRespon.Initial, catRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Save failed ---");
                Trace.WriteLine("Fail message : " + respon.Message);
            }
            Trace.WriteLine("--- End Testing Update Category Repo ---");
        }

        [TestMethod]
        public void TestCategoryDelete()
        {
            Trace.WriteLine("--- Start Testing Delete Category Repo ---");
            CategoryViewModel category = CategoryRepo.byId(5);
            ResponseResult respon = CategoryRepo.Delete(category);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Delete Successful ---");
                CategoryViewModel catRespon = (CategoryViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", catRespon.Initial, catRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Delete failed ---");
                Trace.WriteLine("Fail message : " + respon.Message);
            }
            Trace.WriteLine("--- End Testing Delete Category Repo ---");
        }
    }
}
