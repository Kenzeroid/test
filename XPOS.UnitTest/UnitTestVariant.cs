﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XPOS.DataAccess;
using XPOS.ViewModel;

namespace XPOS.UnitTest
{
    [TestClass]
    public class UnitTestVariant
    {
        [TestMethod]
        public void TestInsertVariant()
        {
            Trace.WriteLine("--- Start Testing Insert Variant Repo ---");
            VariantViewModel variant = new VariantViewModel();
            variant.CategoryId = 1;
            variant.Initial = "TST";
            variant.Name = "Testing";
            ResponseResult respon = VariantRepo.Update(variant);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Save Success ---");
                VariantViewModel varRespon = (VariantViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", varRespon.Initial, varRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Save failed ---");
                Trace.WriteLine("Fail message : " + respon.Message);
            }
            Trace.WriteLine("--- End Testing Insert Variant Repo ---");
        }

        [TestMethod]
        public void TestVariantRepo()
        {
            Trace.WriteLine("--- Start Testing Variant Repo ---");
            List<VariantViewModel> result = VariantRepo.All();
            foreach (var item in result)
            {
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", item.Initial, item.Name));
            }
            Trace.WriteLine("--- End Testing Variant Repo ---");
        }

        [TestMethod]
        public void TestVariantById()
        {
            Trace.WriteLine("--- Start Testing Category Repo ---");
            VariantViewModel result = VariantRepo.byId(10);
            Trace.WriteLine(String.Format("Id : {2} Initial : {0}, Name : {1} ", result.Initial, result.Name, result.Id));
            Trace.WriteLine("--- End Testing Category Repo ---");
        }

        [TestMethod]
        public void TestVariantUpdate()
        {
            Trace.WriteLine("--- Start Testing Update Category Repo ---");
            VariantViewModel variant = new VariantViewModel();
            //category.Initial = "TST";
            variant.Name = "Testing";
            variant.Id = 10;
            ResponseResult respon = VariantRepo.Update(variant);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Save Successful ---");
                VariantViewModel varRespon = (VariantViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", varRespon.Initial, varRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Save failed ---");
                Trace.WriteLine("Fail message : " + respon.Message);
            }
            Trace.WriteLine("--- End Testing Update Variant Repo ---");
        }

        [TestMethod]
        public void TestCategoryDelete()
        {
            Trace.WriteLine("--- Start Testing Delete Category Repo ---");
            CategoryViewModel category = CategoryRepo.byId(5);
            ResponseResult respon = CategoryRepo.Delete(category);
            if (respon.Success == true)
            {
                Trace.WriteLine("--- Delete Successful ---");
                VariantViewModel catRespon = (VariantViewModel)respon.Entity;
                Trace.WriteLine(String.Format("Initial : {0}, Name : {1} ", catRespon.Initial, catRespon.Name));
            }
            else
            {
                Trace.WriteLine("--- Delete failed ---");
                Trace.WriteLine("Fail message : " + respon.Message);
            }
            Trace.WriteLine("--- End Testing Delete Category Repo ---");
        }
    }
}
